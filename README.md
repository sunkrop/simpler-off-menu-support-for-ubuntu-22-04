
# Simpler Off Menu - support for Ubuntu 22.04

Update from 2022.05.02. Release v.7 
  
This version will work only with Ubuntu 22.04 (also may be with 21.10 and may be with future releases).
- "Power Off" button handler changed to "systemctl poweroff" instead of standard Ubuntu's handler with confirmation.
- Added "Reboot" button with handler "systemctl reboot".
- Handler for "Suspend" button changed to "systemctl suspend".
- "Hibernate" button handler changed from "systemctl hibernate" to "sudo systemctl hibernate" since this command requires sudo permissions. "systemctl hibernate" need to add to sudoers list (sudo visudo, user    ALL=NOPASSWD: /usr/bin/systemctl hibernate) to remove user confirmation for performing the command.
- Added separators between buttons.
- To the file "extension_rus.js" added (hard coded) Russian translations for not localized menu items (Reboot, Hibernate). To apply translations this file should be used instead of "extension.js" (need to rename extension_rus.js to extension.js). In the future it is better to support proper localization approach.
- "shell-version" in "metadata.json" file changed from
  "shell-version": ["3.36"]
  to
  "shell-version": ["3.36","3.37","3.38","40.0","41.0","42.0","43.0","44.0","45.0","46.0","47.0","48.0","49.0","50.0"]
  to support Ubuntu 22.04 and other releases. There is no way to specify "all" versions as supported. Because of this many of possible versions were listed. 42.0 means Ubuntu 22.04. 3.36 means Ubuntu 20.04.
- In "prefs.js" file constructor for new Gtk.Grid(...) replaced on empty constructor and setter methods since constructor with parameters is missed in GTK for Ubuntu 22.04. Backwords compatibility is not yet supported. In the future it is also recommended to support case for Ubuntu 20.04.

Link to master verison - https://gitlab.com/K3rcus/simpler-off-menu

![Simpler Off Menu](https://gitlab.com/sunkrop/simpler-off-menu-support-for-ubuntu-22-04/-/raw/master/SOM_v7_on_Ubuntu_22.04.png)

