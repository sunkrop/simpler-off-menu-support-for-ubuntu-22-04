# CHANGELOG
### v. 7
##### Released on 2022-05-02
This version will work only with Ubuntu 22.04 (and may be with 21.10).
- "Power Off" button moved out from submenu.
- "Power Off" button handler changed to "systemctl poweroff" instead of standard Ubuntu's handler with confirmation.
- Added "Reboot" button with handler "systemctl reboot".
- Handler for "Suspend" button changed to "systemctl suspend".
- "Hibernate" button handler changed from "systemctl hibernate" to "sudo systemctl hibernate" since this command requires sudo permissions. "systemctl hibernate" need to add to sudoers list (sudo visudo, user    ALL=NOPASSWD: /usr/bin/systemctl hibernate) to remove user confirmation for performing the command.
- Added separators between buttons.
- To the file "extension_rus.js" added (hard coded) Russian translations for not localized menu items (Reboot, Hibernate). To apply translations this file should be used instead of "extension.js" (need to rename extension_rus.js to extension.js). In the future it is better to support proper localization approach.
- "shell-version" in "metadata.json" file changed from
  "shell-version": ["3.36"]
  to
  "shell-version": ["3.36","3.37","3.38","40.0","41.0","42.0","43.0","44.0","45.0","46.0","47.0","48.0","49.0","50.0"]
  to support Ubuntu 22.04 and other releases. There is no way to specify "all" versions as supported. Because of this many of possible versions were listed. 42.0 means Ubuntu 22.04. 3.36 means Ubuntu 20.04.
- In "prefs.js" file constructor for new Gtk.Grid(...) replaced on empty constructor and setter methods since constructor with parameters is missed in GTK for Ubuntu 22.04. Backwords compatibility is not yet supported. In the future it is also recommended to support case for Ubuntu 20.04.

### v. 6  
##### Released on 2020-6-6  

  - Fixed a Log Out and Power Off names to show it on the system language 
  - Added a option to Suspend in the Menu.
  - Added a option to Hibernate in the Menu.
  - Updated the expansion settings to allow the user set their preference on Gnome Tweaks options.

### v. 5  
##### Released on 2020-5-30  

  - Fixed a bug where changing options bring a error 
  - Added a option to Logout from the Menu.
  - Added a option to merge Logout and PowerOff in just one button, launching with left-click and right-click.
  - Updated the expansion settings to allow the user set their preference on Gnome Tweaks options.

### v. 4  
##### Released on 2020-5-22  

  - Allow to toggle Settings and Lock independently.
  - Added a option to launch Gnome-tweaks from the Menu.
  - Added a option to merge Settings and Gnome-tweaks in just one button, launching with left-click and right-click.
  - Updated the expansion settings to allow the user set their preference on Gnome Tweaks options.

### v. 3  
##### Released on 2020-5-21  

  - Restructuring and cleaning of the code, use strict mode and Lang deprecated in favour of ES6 syntax.
  - This version does not add any improvements for users, so it will not be sent to gnome.extensions

### v. 2  
##### Realeased on 2020-5-13

 - Added a option to keep showing the Settings and Block options in the menu 
 
### v. 1  
##### Realeased on 2020-5-5
  
 - First version launched 
___

